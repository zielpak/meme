package com.bandurski.piotr.meme.Fragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

import com.bandurski.piotr.meme.R;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.squareup.picasso.Picasso;

import java.io.File;

/**
 * Created by Piotr on 13/04/2016.
 */
public class ShowMemeDialogFragment extends DialogFragment {

    ImageView meme_imageView;
    File meme_image;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.popup_show_meme, container);
        meme_imageView=(ImageView)view.findViewById(R.id.image_popup);
        Picasso.with(getActivity()).load(meme_image).into(meme_imageView);
        return view;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    public void  setMeme_image(File file){
        meme_image=file;
    }


}