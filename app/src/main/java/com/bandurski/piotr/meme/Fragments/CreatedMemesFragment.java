package com.bandurski.piotr.meme.Fragments;

import android.app.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.bandurski.piotr.meme.Adapters.CreatedMemesAdapter;
import com.bandurski.piotr.meme.R;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Piotr on 08/04/2016.
 */
public class CreatedMemesFragment extends Fragment{

    private ArrayList<File> created_memes;
    ListView listView;
    View view;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle saveInstanceState) {
        view = inflater.inflate(R.layout.created_memes_fragment, parent, false);
        created_memes=loadImagesAsFilesFromStorage();
        setupListView();
        showCreatedMemes();
        hideTextViewIfThereIsMemes();
        return view;
    }

    @Override
    public void onResume(){
        created_memes=loadImagesAsFilesFromStorage();
        showCreatedMemes();
        super.onResume();
    }

    private void hideTextViewIfThereIsMemes(){
        if(created_memes.size()>0){
            TextView textView=(TextView) view.findViewById(R.id.there_is_no_memes_text_view);
            textView.setVisibility(View.INVISIBLE);
        }
    }

    private void setupListView(){
        listView=(ListView) view.findViewById(R.id.created_memes_list_view);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ShowMemeDialogFragment showMemeDialogFragment=new ShowMemeDialogFragment();
                showMemeDialogFragment.setMeme_image(created_memes.get(position));
                showMemeDialogFragment.show(getFragmentManager(),"");
            }
        });
    }

    private void showCreatedMemes(){
        CreatedMemesAdapter adapter=new CreatedMemesAdapter(getActivity(),created_memes);
        listView.setAdapter(adapter);
    }

    private ArrayList<File> loadImagesAsFilesFromStorage() {
        ArrayList<File> memes=new ArrayList<>();
        try {
            File directory = getActivity().getExternalFilesDir(null);
            for (File file:listFilesForFolder(directory)){
                memes.add(file);
            }
            return memes;
        }
        catch (Exception e) {
            e.printStackTrace();
            return new ArrayList<>();
        }
    }

    private ArrayList<File> listFilesForFolder(final File folder) {
        ArrayList<File>files=new ArrayList<>();
        for (final File fileEntry : folder.listFiles()) {
            if (fileEntry.isDirectory()) {
                listFilesForFolder(fileEntry);
            } else {
                files.add(fileEntry);
            }
        }
        return files;
    }


}
