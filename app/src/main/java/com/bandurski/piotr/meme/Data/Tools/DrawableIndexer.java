package com.bandurski.piotr.meme.Data.Tools;

import android.content.Context;
import android.graphics.drawable.Drawable;

import junit.framework.Assert;

/**
 * Created by Piotr on 11/04/2016.
 */
public class DrawableIndexer {

    public static int getDrawableByNameAsInt(Context context, String name){
        Assert.assertNotNull(context);
        Assert.assertNotNull(name);
        return context.getResources().getIdentifier(name,
                "drawable", context.getPackageName());
    }

    public static Drawable getDrawableByNameAsDrawable(Context context, String name){
        return context.getResources().getDrawable(getDrawableByNameAsInt(context,name));
    }

}