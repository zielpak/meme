package com.bandurski.piotr.meme.Fragments;


import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

import com.bandurski.piotr.meme.R;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.facebook.share.widget.ShareDialog;
import com.squareup.picasso.Picasso;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import java.io.File;
import java.util.List;


/**
 * Created by Piotr on 12/04/2016.
 */
public class FinishMemeDialogFragment extends DialogFragment {

    ImageView meme_imageView;
    ImageView facebook_imageView;
    ImageView twitter_imageView;
    ImageView mail_imageView;

    Bitmap meme_image;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.popup_finish_meme, container);
        bindControls(view);
        Picasso.with(getActivity()).load(getArguments().getString("photo_path")).into(meme_imageView);
        setListeners();
        return view;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    private void bindControls(View v){
        meme_imageView=(ImageView) v.findViewById(R.id.popup_finish_meme_miniature);
        facebook_imageView=(ImageView) v.findViewById(R.id.icon_popup_facebook_share);
        twitter_imageView=(ImageView) v.findViewById(R.id.icon_popup_twitter_share);
        mail_imageView=(ImageView) v.findViewById(R.id.icon_popup_more_share);
    }

    private void setListeners(){
        facebook_imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareToFacebook();
            }
        });
        twitter_imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareToTwitter();
            }
        });
        mail_imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareToEmail();
            }
        });
    }

    private void shareToFacebook(){
        SharePhoto photo = new SharePhoto.Builder()
                .setBitmap(meme_image)
                .build();
        SharePhotoContent content = new SharePhotoContent.Builder().addPhoto(photo).build();
        ShareDialog.show(this, content);
    }

    private void shareToTwitter(){
        Uri myImageUri=Uri.parse(getArguments().getString("photo_path"));
        TweetComposer.Builder builder = new TweetComposer.Builder(getActivity())
                .text("Just created new Meme.")
                .image(myImageUri);
        builder.show();
    }


    private void shareToEmail(){
        Uri uri=Uri.parse(getArguments().getString("photo_path"));
        Intent emailIntent = new Intent(Intent.ACTION_SEND);
        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("message/rfc822");
        emailIntent.putExtra(Intent.EXTRA_SUBJECT,"Hey Bro! I just created new meme");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Look at this");
        emailIntent.putExtra(Intent.EXTRA_STREAM, uri);
        emailIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);

        startActivity(emailIntent);

    }


    public void  setMeme_image(Bitmap bitmap){
        meme_image=bitmap;
    }


}