package com.bandurski.piotr.meme.Fragments;

import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bandurski.piotr.meme.Data.Meme;
import com.bandurski.piotr.meme.Data.Tools.DrawableIndexer;
import com.bandurski.piotr.meme.NewMemeActivity;
import com.bandurski.piotr.meme.R;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;

import java.util.ArrayList;


/**
 * Created by Piotr on 08/04/2016.
 */
public class NewMemeImagePickerFragment extends Fragment  {

    public ArrayList<Meme> memes;

    SliderLayout sliderShow;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle saveInstanceState) {
        View view = inflater.inflate(R.layout.image_picker_fragment, parent, false);
        bindControls(view);
        memes = getMemes();
        addMemesToSlideView();
        return view;
    }


    private void bindControls(View v) {
        sliderShow = (SliderLayout) v.findViewById(R.id._image_picker_slider);
        sliderShow.stopAutoCycle();
    }

    private void addMemesToSlideView() {
        for (int i = 0; i < memes.size(); i++) {
            TextSliderView textSliderView = new TextSliderView(getActivity());
            int meme_drawable_id = DrawableIndexer.getDrawableByNameAsInt(getActivity(), memes.get(i).getDrawable_name());

            textSliderView.image(meme_drawable_id).
                    setScaleType(BaseSliderView.ScaleType.CenterCrop).
                    setOnSliderClickListener(new SliderClickListener(i, this));

            sliderShow.addSlider(textSliderView);
        }
    }

    public void onImageSelected(Bitmap bitmap) {
        NewMemeActivity activity = (NewMemeActivity) getActivity();
        activity.setMemePicture(bitmap);
        activity.navigateToMemeEditor();
    }

    private ArrayList<Meme> getMemes() {
        ArrayList<Meme> memes = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            String image_name = "meme" + i;
            Meme meme = new Meme(image_name);
            memes.add(meme);
        }
        return memes;
    }


}


class SliderClickListener implements BaseSliderView.OnSliderClickListener {

    int meme_id;
    NewMemeImagePickerFragment fragment;

    SliderClickListener(int id, NewMemeImagePickerFragment picker) {
        this.meme_id = id;
        fragment = picker;
    }

    int getId() {
        return meme_id;
    }

    @Override
    public void onSliderClick(BaseSliderView slider) {
        Bitmap selected_image = ((BitmapDrawable) DrawableIndexer.getDrawableByNameAsDrawable(fragment.getActivity(),
                fragment.memes.get(meme_id).getDrawable_name())).getBitmap();
        fragment.onImageSelected(selected_image);
    }
}