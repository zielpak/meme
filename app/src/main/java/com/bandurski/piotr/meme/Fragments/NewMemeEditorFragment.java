package com.bandurski.piotr.meme.Fragments;

import android.Manifest;
import android.app.Fragment;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.bandurski.piotr.meme.Data.Tools.TextToImageWriter;
import com.bandurski.piotr.meme.NewMemeActivity;
import com.bandurski.piotr.meme.R;
import com.gc.materialdesign.views.ButtonRectangle;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.security.SecureRandom;

import yuku.ambilwarna.AmbilWarnaDialog;

/**
 * Created by Piotr on 08/04/2016.
 */
public class NewMemeEditorFragment extends Fragment {

    public static final int WRITE_DATA_REQUEST_CODE = 125;

    Bitmap editing_image;
    TextToImageWriter textToImageWriter;

    ImageView edit_image;
    EditText first_line_edit_text;
    EditText second_line_edit_text;
    ButtonRectangle first_line_color_button;
    ButtonRectangle second_line_color_button;
    Uri meme_path;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle saveInstanceState) {
        View view = inflater.inflate(R.layout.new_meme_fragment, parent, false);
        NewMemeActivity activity = (NewMemeActivity) getActivity();
        activity.showOnlySaveIcon();
        bindControls(view);
        setListeners();
        receivePicture();
        textToImageWriter = new TextToImageWriter(editing_image, getActivity());
        return view;
    }

    public void finishMeme() {
        requestPermissionToWriteStorage();
    }

    private void showFinishMemeDialog() {
        FinishMemeDialogFragment finishMemeDialogFragment = new FinishMemeDialogFragment();
        finishMemeDialogFragment.setMeme_image(textToImageWriter.getBitmap());

        Bundle args = new Bundle();
        if (meme_path != null) {
            args.putString("photo_path", meme_path.toString());
        }
        finishMemeDialogFragment.setArguments(args);

        finishMemeDialogFragment.show(getFragmentManager(), "");
    }

    private void receivePicture() {
        NewMemeActivity activity = (NewMemeActivity) getActivity();
        editing_image = activity.getMemePicture();
        edit_image.setImageBitmap(editing_image);
    }

    private void bindControls(View v) {
        edit_image = (ImageView) v.findViewById(R.id.image_currently_edited);
        first_line_edit_text = (EditText) v.findViewById(R.id.new_meme_text_first_line);
        second_line_edit_text = (EditText) v.findViewById(R.id.new_meme_text_second_line);
        first_line_color_button = (ButtonRectangle) v.findViewById(R.id.new_meme_text_color_first_line);
        second_line_color_button = (ButtonRectangle) v.findViewById(R.id.new_meme_text_color_second_line);
    }

    private void setListeners() {
        first_line_edit_text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                textToImageWriter.setFirstLineText(s.toString());
                setImageToEditingImageView(textToImageWriter.getBitmap());
            }
        });

        second_line_edit_text.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                textToImageWriter.setSecondLineText(s.toString());
                setImageToEditingImageView(textToImageWriter.getBitmap());
            }
        });

        first_line_color_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showColorChooserForFirstLine();
            }
        });

        second_line_color_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showColorChooserForSecondLine();
            }
        });

    }

    private void showColorChooserForFirstLine() {
        AmbilWarnaDialog dialog = new AmbilWarnaDialog(getActivity(), textToImageWriter.getFirst_line_color(), new AmbilWarnaDialog.OnAmbilWarnaListener() {
            @Override
            public void onOk(AmbilWarnaDialog dialog, int color) {
                textToImageWriter.setFirst_line_color(color);
                setImageToEditingImageView(textToImageWriter.getBitmap());
            }

            @Override
            public void onCancel(AmbilWarnaDialog dialog) {
            }
        });
        dialog.show();
    }

    private void showColorChooserForSecondLine() {
        AmbilWarnaDialog dialog = new AmbilWarnaDialog(getActivity(), textToImageWriter.getSecond_line_color(), new AmbilWarnaDialog.OnAmbilWarnaListener() {
            @Override
            public void onOk(AmbilWarnaDialog dialog, int color) {
                textToImageWriter.setSecond_line_color(color);
                setImageToEditingImageView(textToImageWriter.getBitmap());
            }

            @Override
            public void onCancel(AmbilWarnaDialog dialog) {
            }
        });
        dialog.show();
    }

    private void setImageToEditingImageView(Bitmap bitmap) {
        edit_image.setImageBitmap(bitmap);
    }

    private void saveMemeToMemory(Bitmap bitmap) {
        String file_name = new BigInteger(130, new SecureRandom()).toString(32) + ".png";
        File file;
        FileOutputStream outputStream;
        try {
            file = new File(getActivity().getExternalFilesDir(null), file_name);
            outputStream = new FileOutputStream(file);
            meme_path = Uri.fromFile(file);
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream);
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void requestPermissionToWriteStorage() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                System.out.print("");
            } else {
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        WRITE_DATA_REQUEST_CODE);
            }
        } else {
            saveMemeToMemory(textToImageWriter.getBitmap());
            showFinishMemeDialog();
        }
    }

}
