package com.bandurski.piotr.meme;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.menu.MenuItemImpl;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.bandurski.piotr.meme.Fragments.NewMemeEditorFragment;
import com.bandurski.piotr.meme.Fragments.NewMemeImagePickerFragment;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

import java.io.File;

import nl.changer.polypicker.Config;
import nl.changer.polypicker.ImagePickerActivity;


/**
 * Created by Piotr on 08/04/2016.
 */
public class NewMemeActivity extends AppCompatActivity {

    private static final int INTENT_REQUEST_GET_IMAGES = 420;
    private static final int CAMERA_AND_READ_DATA_REQUEST_CODE = 122;

    NewMemeImagePickerFragment memeImagePickerFragment = new NewMemeImagePickerFragment();
    NewMemeEditorFragment memeEditorFragment = new NewMemeEditorFragment();
    MenuItemImpl galery_icon;
    MenuItemImpl save_icon;
    Bitmap meme_picture;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_meme);
        navigateToImageSelector();
        initializeFacebook();
    }

    private void getImages() {
        Intent intent = new Intent(this, ImagePickerActivity.class);
        Config config = new Config.Builder()
                .setTabBackgroundColor(R.color.white)
                .setTabSelectionIndicatorColor(R.color.blue)
                .setCameraButtonColor(R.color.green)
                .setSelectionLimit(1)
                .build();
        ImagePickerActivity.setConfig(config);
        startActivityForResult(intent, INTENT_REQUEST_GET_IMAGES);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);

        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == INTENT_REQUEST_GET_IMAGES) {
                Parcelable[] parcelableUris = intent.getParcelableArrayExtra(ImagePickerActivity.EXTRA_IMAGE_URIS);
                if (parcelableUris == null) {
                    return;
                }
                // Java doesn't allow array casting, this is a little hack
                Uri[] uris = new Uri[parcelableUris.length];
                System.arraycopy(parcelableUris, 0, uris, 0, parcelableUris.length);

                if (uris != null && uris.length > 0) {
                    File image = new File(uris[0].getPath());
                    BitmapFactory.Options bmOptions = new BitmapFactory.Options();
                    Bitmap bitmap = BitmapFactory.decodeFile(image.getAbsolutePath(), bmOptions);
                    bitmap = scaleImage(bitmap);
                    setMemePicture(bitmap);
                    navigateToMemeEditor();
                }
            }
        }
    }

    private Bitmap scaleImage(Bitmap bitmap) {
        int imageWidth = bitmap.getWidth();
        int imageHeight = bitmap.getHeight();

        if (imageHeight > imageWidth) {
            float scaleSize = (float) imageHeight / 500;
            int newImageHeight = Math.round(imageHeight / scaleSize);
            int newImageWidth = Math.round(imageWidth / scaleSize);
            return Bitmap.createScaledBitmap(bitmap, newImageWidth, newImageHeight, true);
        } else {
            float scaleSize = (float) imageWidth / 500;
            int newImageHeight = Math.round(imageHeight / scaleSize);
            int newImageWidth = Math.round(imageWidth / scaleSize);
            return Bitmap.createScaledBitmap(bitmap, newImageWidth, newImageHeight, true);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {

        switch (requestCode) {
            case CAMERA_AND_READ_DATA_REQUEST_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    getImages();
                } else {
                    Toast.makeText(this, "Sorry, I cannt acces to your images", Toast.LENGTH_SHORT).show();
                }
                return;
            }
            case NewMemeEditorFragment.WRITE_DATA_REQUEST_CODE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    memeEditorFragment.finishMeme();
                } else {
                    Toast.makeText(this, "We can't save your meme :(", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_edit_meme, menu);
        galery_icon = (MenuItemImpl) menu.findItem(R.id.action_pick_photo);
        save_icon = (MenuItemImpl) menu.findItem(R.id.action_save_meme);
        showOnlyGalleryIcon();
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_save_meme) {

            memeEditorFragment.finishMeme();
            return true;
        } else if (id == R.id.action_pick_photo) {
            requestPermissionToReadStorageAndCamera();
        }

        return super.onOptionsItemSelected(item);
    }

    public void showOnlyGalleryIcon() {
        galery_icon.setVisible(true);
        save_icon.setVisible(false);
    }

    public void showOnlySaveIcon() {
        galery_icon.setVisible(false);
        save_icon.setVisible(true);
    }

    private void navigateToImageSelector() {
        getFragmentManager().beginTransaction().add(R.id.new_meme_activity_container, memeImagePickerFragment).commit();
    }

    public Bitmap getMemePicture() {
        return meme_picture;
    }

    public void setMemePicture(Bitmap bim) {
        this.meme_picture = bim;
    }

    public void navigateToMemeEditor() {
        getFragmentManager().beginTransaction().replace(R.id.new_meme_activity_container, memeEditorFragment).addToBackStack(null).commit();
    }

    private void initializeFacebook() {
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
    }

    private void requestPermissionToReadStorageAndCamera() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                System.out.print("");
            } else {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                        CAMERA_AND_READ_DATA_REQUEST_CODE);
            }
        } else {
            getImages();
        }
    }
}
