package com.bandurski.piotr.meme.Data.Tools;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;

/**
 * Created by Piotr on 11/04/2016.
 */
public class TextToImageWriter {
    String first_line = "";
    String second_line = "";
    int first_line_color;
    int second_line_color;
    Bitmap editing_image;
    Context context;

    public TextToImageWriter(Bitmap bitmap, Context context) {
        editing_image = bitmap;
        this.context = context;
    }

    public int getSecond_line_color() {
        return second_line_color;
    }

    public void setSecond_line_color(int second_line_color) {
        this.second_line_color = second_line_color;
    }

    public int getFirst_line_color() {
        return first_line_color;
    }

    public void setFirst_line_color(int first_line_color) {
        this.first_line_color = first_line_color;
    }


    public void setFirstLineText(String text) {
        first_line = text;
    }

    public void setSecondLineText(String text) {
        second_line = text;
    }

    public Bitmap getBitmap() {
        Bitmap bit1 = writeTextToImage(first_line, line.FIRST, editing_image);
        Bitmap bit2 = writeTextToImage(second_line, line.SECOND, bit1);
        return bit2;
    }

    private Bitmap writeTextToImage(String gText, line line, Bitmap pic) {
        Resources resources = context.getResources();
        float scale = resources.getDisplayMetrics().density;
        Bitmap bitmap = pic.copy(Bitmap.Config.ARGB_8888, true);

        android.graphics.Bitmap.Config bitmapConfig = bitmap.getConfig();
        if (bitmapConfig == null) {
            bitmapConfig = android.graphics.Bitmap.Config.ARGB_8888;
        }

        bitmap = bitmap.copy(bitmapConfig, true);

        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        //paint.setColor(Color.rgb(255, 255, 255));
        paint.setTextSize((int) (bitmap.getHeight() / 20 * scale));
        paint.setShadowLayer(1f, 0f, 1f, Color.WHITE);

        Rect bounds = new Rect();
        paint.getTextBounds(gText, 0, gText.length(), bounds);
        int x = (bitmap.getWidth() - bounds.width()) / 2;
        int y;

        if (line.equals(TextToImageWriter.line.FIRST)) {
            y = (bitmap.getHeight() + bounds.height()) / 7;
            paint.setColor(Color.rgb(Color.red(first_line_color), Color.green(first_line_color), Color.blue(first_line_color)));
        } else {
            y = (bitmap.getHeight() + bounds.height()) / 7 * 6;
            paint.setColor(Color.rgb(Color.red(second_line_color), Color.green(second_line_color), Color.blue(second_line_color)));
        }
        canvas.drawText(gText, x, y, paint);

        return bitmap;
    }

    enum line {FIRST, SECOND}

}
