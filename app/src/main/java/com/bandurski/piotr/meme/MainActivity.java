package com.bandurski.piotr.meme;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.menu.MenuItemImpl;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import com.bandurski.piotr.meme.Fragments.CreatedMemesFragment;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import io.fabric.sdk.android.Fabric;

public class MainActivity extends AppCompatActivity {

    private static final String TWITTER_KEY = "F2STjeFv1dZZs4mLUZMQg7WPw ";
    private static final String TWITTER_SECRET = "OR4S4jIzrcyXxgzlajjE6zPKnK3QBo5agYUJMhicBFrXpB7OPW ";

    CreatedMemesFragment createdMemesFragment=new CreatedMemesFragment();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Twitter(authConfig));
        setContentView(R.layout.activity_main);
        setupToolbar();
        setupFloatingButton();
        showCreatedMemesFragment();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showCreatedMemesFragment(){
        getFragmentManager().beginTransaction()
                .replace(R.id.main_activity_container,createdMemesFragment)
                    .commit();
    }

    private void setupToolbar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
    }

    private void setupFloatingButton(){
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                navigateToNewMemeCrator();
            }
        });
    }

    private void navigateToNewMemeCrator(){
        Intent intent = new Intent(this, NewMemeActivity.class);
        startActivity(intent);
    }

}
