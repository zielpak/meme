package com.bandurski.piotr.meme.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bandurski.piotr.meme.Data.Meme;
import com.bandurski.piotr.meme.R;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Piotr on 09/04/2016.
 */
public class CreatedMemesAdapter extends ArrayAdapter<File> {

    ImageView imageView;
    File meme;
    public CreatedMemesAdapter(Context context, ArrayList<File> memes) {
        super(context, 0, memes);
    }
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        meme = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.created_meme_adapter, parent, false);
        }
        showImage(convertView);
        return convertView;
    }

    private void showImage(View v){
        imageView=(ImageView) v.findViewById(R.id.created_meme_adapter_image_view);
        Picasso.with(getContext()).load(meme).into(imageView);
    }
}